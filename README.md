## Name
Inventario de vacunación de empleados (employee_vaccination)

## Description
- Aplicación realizada en Angular 13 con Material Designer y persistencia de datos con Fake API (json-server) con autenticación Auth(json-server-auth)

- Diseña con las buenas prácticas, asumiendo que es una aplicación empresarial.

La aplicación cuenta con 2 roles: Administrador y Empleado

Administrador:

    a. Registra la siguiente información del empleado.
              ○ Cédula.
              ○ Nombres.
              ○ Apellidos.
              ○ Correo electrónico.
    b. Los campos cuentan con validaciones de acuerdo al tipo de dato:
              ○ Todos los campos son requeridos.
              ○ Cédula válida. (Incluir un valor numérico y único de 10 dígitos)
              ○ Correo electrónico válido.
              ○ Nombres y apellidos no deben contener números o caracteres especiales.
     c. Al dar de alta un empleado generar un usuario y contraseña para el empleado.
     d. Filtros:
              a. Por estado de vacunación.
              b. Por tipo de vacuna.
              c. Por fecha de vacunación.

Empleado: 

    Permite completar la siguiente información:
              ● Fecha de nacimiento.
              ● Dirección de domicilio.
              ● Teléfono móvil.
              ● Estado de vacunación: Vacunado / No Vacunado.
              ● Si el empleado está en estado vacunado, se debe pedir la siguiente información requerida:
              ○ Tipo de vacuna: Sputnik, AstraZeneca, Pfizer y Jhonson&Jhonson
              ○ Fecha de vacunación.
              ○ Número de dosis.

## Installation
Descargar el proyecto y ejecutar el comando npm install

## Getting started

- Levantar el Fake API en una terminal con el siguiente comando: json-server db.json -m ./node_modules/json-server-auth
- Levantar el Angular en una terminal con el siguiente comando: ng serve
- Rol Administrador: usuario: rveliz@hotmail.com / contraseña: 1234
- Rol Empleado: usuario: lgarcia@hotmail.com / contraseña: 1234

## Author
- Richard Veliz
- richard6veliz@hotmail.com
