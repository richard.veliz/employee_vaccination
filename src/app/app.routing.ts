import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import {SigninComponent} from "app/shared/components/sessions/signin/signin.component";
import {AdminLayoutComponent} from "app/shared/components/layouts/adminlayout/adminlayout.component";
import {AuthGuard} from "app/shared/guards/auth.guard";
import {NotFoundComponent} from "./shared/components/sessions/notfound/notfound.component";

const routes: Routes =[
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {path: 'signin', component: SigninComponent},
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [{
      path: '',
      loadChildren: () => import('./modules/modules.module').then(m => m.ModulesModule)
    }]
  },
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
