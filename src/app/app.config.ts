export class AppConfig {
  public static URL_API_DOMAIN = 'http://localhost:3000';
  public static URL_TOKEN = AppConfig.URL_API_DOMAIN + '/login';
  public static ENCRYPT_STORAGE = false;
}
