import {Routes} from '@angular/router';
import {TableemployeeComponent} from "./components/employee/tableemployee/tableemployee.component";
import {UpdateemployeeComponent} from "./components/employee/updateemployee/updateemployee.component";


export const VaccinationRoutes: Routes = [
  {path: 'tableemployee', component: TableemployeeComponent},
  {path: 'updateemployee', component: UpdateemployeeComponent},
];
