import {Constants} from 'app/shared/constants';

export class ConstantsVaccionation extends Constants {
  public static STATUS_VACCINE = 1;
  public static STATUS_NOT_VACCINE = 2;
}

