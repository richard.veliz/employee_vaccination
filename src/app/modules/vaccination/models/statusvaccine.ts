export class Statusvaccine {
  public id: number;
  public descripcion: string;

  constructor(object) {
    if (object) {
      this.id = (object.id) ? object.id : null;
      this.descripcion = (object.descripcion) ? object.descripcion : null;
    }
  }
}
