import {ConstantsVaccionation} from '../constants';
import * as moment from "moment";
import {Moment} from "moment";

export class EmployeeVaccine {
  public id: number;
  public identification: number;
  public names: string;
  public surnames: string;
  public email: string;
  public address: string;
  public birthdate: string;
  public mobile: string;
  public statusvaccineId: number;
  public typeVaccineId: number;
  public vaccinedate: string;
  public numberdoses: string;

  constructor(obj?) {
    if (obj) {
      this.id = (obj.id) ? obj.id : null;
      this.identification = (obj.identification) ? obj.identification : null;
      this.names = (obj.names) ? obj.names : null;
      this.surnames = (obj.surnames) ? obj.surnames : null;
      this.email = (obj.email) ? obj.email : null;
      this.address = (obj.address) ? obj.address : null;
      this.birthdate = (obj.birthdate) ? moment(obj.birthdate, 'DD-MM-YYYY').format('DD-MM-YYYY') : null;
      this.mobile = (obj.mobile) ? obj.mobile : null;
      this.statusvaccineId = (obj.statusvaccineId) ? obj.statusvaccineId : ConstantsVaccionation.STATUS_NOT_VACCINE;
      this.typeVaccineId = (obj.typeVaccineId) ? obj.typeVaccineId : null;
      this.vaccinedate = (obj.vaccinedate) ? moment(obj.vaccinedate, 'DD-MM-YYYY').format('DD-MM-YYYY') : null;
      this.numberdoses = (obj.numberdoses) ? obj.numberdoses : null;
    }
  }
}
