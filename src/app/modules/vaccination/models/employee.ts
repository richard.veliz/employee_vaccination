import {ConstantsVaccionation} from '../constants';

export class Employee {
  public id: number;
  public identification: number;
  public names: string;
  public surnames: string;
  public email: string;
  public statusvaccineId: number;

  constructor(obj?) {
    if (obj) {
      this.id = (obj.id) ? obj.id : null;
      this.identification = (obj.identification) ? obj.identification : null;
      this.names = (obj.names) ? obj.names : null;
      this.surnames = (obj.surnames) ? obj.surnames : null;
      this.email = (obj.email) ? obj.email : null;
      this.statusvaccineId = (obj.statusvaccineId) ? obj.statusvaccineId : ConstantsVaccionation.STATUS_NOT_VACCINE;
    }
  }
}
