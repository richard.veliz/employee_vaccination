import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {EmployeeService} from 'app/modules/vaccination/services/employe.service';
import {DatasourceService} from 'app/shared/services/datasource.service';
import {PaginatorComponent} from 'app/shared/components/gui/datatable/paginator/paginator.component';
import {FormControl} from '@angular/forms';
import {WhereInterface} from 'app/shared/interfaces/where.interface';
import {DialogemployeeComponent} from '../dialogemployee/dialogemployee.component';
import {DialogService} from 'app/shared/services/dialog.service';
import {Subscription} from 'rxjs';
import {Statusvaccine} from "../../../models/statusvaccine";
import {Typevaccine} from "../../../models/typevaccine";
import {StatysvaccineService} from "../../../services/statysvaccine.service";
import {TypevaccineService} from "../../../services/typevaccine.service";
import * as moment from "moment";

@Component({
  selector: 'app-table-employee',
  templateUrl: './tableemployee.component.html',
  styleUrls: ['./tableemployee.component.css']
})
export class TableemployeeComponent implements OnInit, OnDestroy {

  dataSource: DatasourceService;
  displayedColumns: string[] = ['identification', 'names', 'surnames', 'email', 'action'];

  @ViewChild('paginator', {static: true}) paginator: PaginatorComponent;

  identification: FormControl;
  name: FormControl;
  surname: FormControl;
  email: FormControl;
  statusvaccineId: FormControl;
  typeVaccineId: FormControl;
  vaccinedate: FormControl;

  listStatus: Statusvaccine[] = [];
  listType: Typevaccine[] = [];
  maxDate = new Date();

  private deleteSubscription: Subscription;

  constructor(private employeeService: EmployeeService,
              private statysvaccineService: StatysvaccineService,
              private typevaccineService: TypevaccineService,
              private dialogService: DialogService) {
  }

  ngOnInit() {
    this.identification = new FormControl();
    this.name = new FormControl();
    this.surname = new FormControl();
    this.email = new FormControl();
    this.statusvaccineId = new FormControl();
    this.typeVaccineId = new FormControl();
    this.vaccinedate = new FormControl();
    this.statysvaccineService.list().then(status => this.listStatus = status);
    this.typevaccineService.list().then(types => this.listType = types);
    this.dataSource = new DatasourceService(this.employeeService);
    this.dataSource.loadPage(1);
    this.dataSource.refreshPageSubject.subscribe(() => this.refreshPage(1));
    this.dataSource.changePage.subscribe(() => this.refreshPage());
  }

  ngOnDestroy() {
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe();
    }
  }

  refreshPage(pageIndex?: number) {
    const where: WhereInterface[] = [];
    const names = this.name.value;
    const identification = this.identification.value;
    const surname = this.surname.value;
    const email = this.email.value;
    const statusvaccineId = this.statusvaccineId.value;
    const typeVaccineId = this.typeVaccineId.value;
    const vaccinedate = this.vaccinedate.value;

    if (statusvaccineId) {
      where.push({key: 'statusvaccineId', value: statusvaccineId});
    }
    if (typeVaccineId) {
      where.push({key: 'typeVaccineId', value: typeVaccineId});
    }
    if (vaccinedate) {
      where.push({key: 'vaccinedate', value: moment(vaccinedate).format('DD-MM-YYYY')});
    }
    if (identification) {
      where.push({key: 'identification_like', value: identification});
    }
    if (names) {
      where.push({key: 'names_like', value: names});
    }
    if (surname) {
      where.push({key: 'surname_like', value: surname});
    }
    if (email) {
      where.push({key: 'email_like', value: email});
    }

    this.dataSource.refreshPage(this.paginator.paginator, pageIndex, where);
  }

  openDialog(id: number = 0): void {
    this.dialogService.open(DialogemployeeComponent, 60, {id});
    this.dialogService.dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.refreshPage(1);
      }
    });
  }

  delete(id: number): void {
    this.dialogService.delete(this.employeeService, id, 35, '¿Esta seguro que desea eliminar el empleado?', null, 'Empleado eliminado correctamente.');
    this.deleteSubscription = this.dialogService.dialogDelete.afterClosed().subscribe(status => {
      if (status === 1) {
        this.refreshPage(1);
      }
    });
  }

  dateRangeChange(dateRangeStart: HTMLInputElement, dateRangeEnd: HTMLInputElement) {
    console.log(dateRangeStart.value);
    console.log(dateRangeEnd.value);
  }
}
