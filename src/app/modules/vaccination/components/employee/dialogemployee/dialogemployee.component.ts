import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import * as coreValidators from 'app/shared/helpers/additional-methods.validator';
import {FormService} from 'app/shared/services/form.service';
import {MessageService} from 'app/shared/services/message.service';
import {SpinnerViewService} from 'app/shared/services/spinnerview.service';
import {Router} from '@angular/router';
import {EmployeeService} from 'app/modules/vaccination/services/employe.service';
import {Employee} from 'app/modules/vaccination/models/employee';
import {UserService} from 'app/shared/services/user.service';
import {User} from 'app/shared/models/user';
import {ConstantsVaccionation} from 'app/modules/vaccination/constants';

@Component({
  selector: 'app-dialog-employee',
  templateUrl: './dialogemployee.component.html',
  styleUrls: ['./dialogemployee.component.css']
})

export class DialogemployeeComponent implements OnInit {
  form: FormGroup;
  id: number = this.data.id;

  constructor(public dialogRef: MatDialogRef<DialogemployeeComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private employeeService: EmployeeService,
              private userService: UserService,
              private messageService: MessageService,
              private spinnerViewService: SpinnerViewService,
              private router: Router,
              private formService: FormService) {
  }

  ngOnInit() {
    this.createForm();
    if (this.id > 0) {
      this.spinnerViewService.start(null, false);
      this.employeeService.find(this.id)
        .then(result => {
          this.form.patchValue({...result});
          this.spinnerViewService.stop();
        }).catch(error => {
        this.messageService.openSnackBar('Hubo un inconveniente al consultar la sucursal, ' +
          'por favor comuniquese con el administrador.', null);
        console.log(error);
        this.spinnerViewService.stop();
      });
    }
  }

  createForm() {
    this.form = new FormGroup({
      identification: new FormControl(null, [
        Validators.required,
        Validators.maxLength(10),
        coreValidators.ValidateIdentityCard
      ]),
      names: new FormControl(null, Validators.required),
      surnames: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
    });
  }

  save() {
    if (this.form.valid) {
      this.spinnerViewService.start(null, false);
      const employee = new Employee({...this.form.value, id: this.id});
      if (this.id > 0) {
        this.employeeService.update(this.id, employee)
          .then(result => {
            this.messageService.openSnackBar('Empleado actualizado correctamente.', null);
            this.spinnerViewService.stop();
            this.close(result);
          }).catch(error => {
          this.spinnerViewService.stop();
          this.messageService.openSnackBar('Hubo un inconveniente al actualizar el empleado, por favor comuniquese con el administrador.');
          console.log(error);
        });
      } else {
        this.employeeService.create(employee)
          .then(result => {
            const user = new User({
              rolId: ConstantsVaccionation.ROL_EMPLOYEE,
              name: employee.names + ' ' + employee.surnames,
              email: employee.email,
              password: employee.identification,
              identification: employee.identification
            })
            this.userService.create(user).then(res => {
              this.spinnerViewService.stop()
              this.messageService.openSnackBar('Empleado guardado correctamente. Usuario: '
                + user.email + ' Contraseña: ' + user.password, null);
              this.spinnerViewService.stop();
              this.close(result);
            });

          }).catch(error => {
          this.spinnerViewService.stop();
          this.messageService.openSnackBar('Hubo un inconveniente al guardar el empleado, por favor comuniquese con el administrador.');
          console.log(error);
        });
      }
    } else {
      this.formService.validateAllFormFields(this.form);
    }
  }

  close = (result?): void => this.dialogRef.close(result);
}
