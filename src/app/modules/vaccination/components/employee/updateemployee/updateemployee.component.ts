import {Component, OnInit} from '@angular/core';
import {SessionService} from "app/shared/services/session.service";
import {EmployeeService} from "../../../services/employe.service";
import {EmployeeVaccine} from "../../../models/employeevaccine";
import {SpinnerViewService} from "app/shared/services/spinnerview.service";
import {MessageService} from "app/shared/services/message.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import * as coreValidators from "app/shared/helpers/additional-methods.validator";
import {FormService} from "app/shared/services/form.service";
import * as moment from 'moment';
import {Statusvaccine} from "../../../models/statusvaccine";
import {StatysvaccineService} from "../../../services/statysvaccine.service";
import {Typevaccine} from "../../../models/typevaccine";
import {TypevaccineService} from "../../../services/typevaccine.service";
import {ConstantsVaccionation} from "../../../constants";

@Component({
  selector: 'app-update-employee',
  templateUrl: './updateemployee.component.html',
  styleUrls: ['./updateemployee.component.css']
})
export class UpdateemployeeComponent implements OnInit {

  id: number;
  form: FormGroup;
  maxDate = new Date();
  listStatus: Statusvaccine[] = [];
  listType: Typevaccine[] = [];
  constants = ConstantsVaccionation;

  constructor(private sesionService: SessionService,
              private spinnerViewService: SpinnerViewService,
              private messageService: MessageService,
              private formService: FormService,
              private employeeService: EmployeeService,
              private statysvaccineService: StatysvaccineService,
              private typevaccineService: TypevaccineService) {
  }

  ngOnInit() {
    this.spinnerViewService.start();
    this.createForm();
    this.statysvaccineService.list().then(status => this.listStatus = status);
    this.typevaccineService.list().then(types => this.listType = types);
    this.employeeService.findIdentificacion(this.sesionService.getUser().identification)
      .then(response => {
        if (response) {
          const employee = response[0];
          this.id = employee.id;
          if (employee.birthdate) {
            employee.birthdate = moment(employee.birthdate, 'DD-MM-YYYY');
          }
          if (employee.vaccinedate) {
            employee.vaccinedate = moment(employee.vaccinedate, 'DD-MM-YYYY');
          }
          this.form.patchValue({...employee});
        }
        this.spinnerViewService.stop();
      }).catch(error => {
      this.spinnerViewService.stop();
      this.messageService.showError("Hubo un error al obtener los datos");
    });
  }

  createForm() {
    this.form = new FormGroup({
      identification: new FormControl(null, [
        Validators.required,
        Validators.maxLength(10),
        coreValidators.ValidateIdentityCard
      ]),
      names: new FormControl(null, Validators.required),
      surnames: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.email]),
      address: new FormControl(null, Validators.maxLength(50)),
      birthdate: new FormControl(null, Validators.required),
      mobile: new FormControl(null, [Validators.maxLength(10), coreValidators.phone('')]),
      statusvaccineId: new FormControl(null, Validators.required),
      typeVaccineId: new FormControl(),
      vaccinedate: new FormControl(),
      numberdoses: new FormControl(),
    });
  }

  changeStatus(status_id: number) {
    const typeVaccineId = this.form.get('typeVaccineId') as FormControl;
    const vaccinedate = this.form.get('vaccinedate') as FormControl;
    const numberdoses = this.form.get('numberdoses') as FormControl;
    if (status_id == this.constants.STATUS_VACCINE) {
      this.formService.addRequired(typeVaccineId);
      this.formService.addRequired(vaccinedate);
      this.formService.addRequired(numberdoses);
      typeVaccineId.setValue(null);
      vaccinedate.setValue(null);
      numberdoses.setValue(null);
    } else {
      this.formService.removeValidator(typeVaccineId);
      this.formService.removeValidator(vaccinedate);
      this.formService.removeValidator(numberdoses);
    }
  }

  save() {
    if (this.form.valid) {
      this.spinnerViewService.start(null, false);
      const employee = new EmployeeVaccine({...this.form.value, id: this.id});
      this.employeeService.update(this.id, employee)
        .then(result => {
          this.messageService.openSnackBar('Información actualizada correctamente.', null);
          this.spinnerViewService.stop();
        }).catch(error => {
        this.spinnerViewService.stop();
        this.messageService.openSnackBar('Hubo un inconveniente al actualizar la información, por favor comuniquese con el administrador.');
        console.log(error);
      });
    } else {
      this.formService.validateAllFormFields(this.form);
    }
  }
}
