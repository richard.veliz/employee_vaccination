import {Injectable} from '@angular/core';
import {ManagerService} from 'app/shared/services/manager.service';
import {AppConfig} from 'app/app.config';
import {PaginationInterface} from 'app/shared/interfaces/pagination.interface';
import {Employee} from '../models/employee';
import {EmployeeVaccine} from "../models/employeevaccine";

@Injectable()
export class EmployeeService {

  private url = AppConfig.URL_API_DOMAIN + '/employees';

  constructor(private managerService: ManagerService) {
  }

  collection = (pagination: PaginationInterface): Promise<any> => this.managerService.collection(this.url, pagination);

  find = (id: number): Promise<any> => this.managerService.get(`${this.url}/${id}`);

  findIdentificacion = (identificacion: number): Promise<any> => this.managerService.get(`${this.url}?identification=${identificacion}`);

  create = (employee: Employee | EmployeeVaccine): Promise<any> => this.managerService.post(this.url, employee);

  update = (id: number, employee: Employee | EmployeeVaccine): Promise<any> => this.managerService.put(`${this.url}/${id}`, employee);

  delete = (id: number): Promise<any> => this.managerService.delete(`${this.url}/${id}`);
}
