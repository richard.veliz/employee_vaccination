import {Injectable} from '@angular/core';
import {ManagerService} from 'app/shared/services/manager.service';
import {AppConfig} from 'app/app.config';

@Injectable()
export class StatysvaccineService {

  private url = AppConfig.URL_API_DOMAIN + '/statusvaccines';

  constructor(private managerService: ManagerService) {
  }

  list = (): Promise<any> => this.managerService.get(this.url);
}
