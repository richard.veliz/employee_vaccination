import {NgModule} from '@angular/core';
import {TableemployeeComponent} from './components/employee/tableemployee/tableemployee.component';
import {VaccinationRoutes} from './vaccination.routing';
import {RouterModule} from '@angular/router';
import {EmployeeService} from './services/employe.service';
import {DialogemployeeComponent} from './components/employee/dialogemployee/dialogemployee.component';
import {HttpClientModule} from '@angular/common/http';
import {DialogService} from 'app/shared/services/dialog.service';
import {UserService} from 'app/shared/services/user.service';
import {UpdateemployeeComponent} from "./components/employee/updateemployee/updateemployee.component";
import {ModulesModule} from "../modules.module";
import {StatysvaccineService} from "./services/statysvaccine.service";
import {TypevaccineService} from "./services/typevaccine.service";

const components = [
  DialogemployeeComponent,
  TableemployeeComponent,
  UpdateemployeeComponent,
];

@NgModule({
  imports: [
    ModulesModule,
    HttpClientModule,
    RouterModule.forChild(VaccinationRoutes),
  ],
  declarations: [components],
  providers: [
    DialogService,
    UserService,
    EmployeeService,
    StatysvaccineService,
    TypevaccineService,
  ]
})

export class VaccinationModule {
}
