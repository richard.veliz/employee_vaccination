import {Routes} from '@angular/router';

import {DashboardComponent} from '../shared/components/dashboard/dashboard.component';

export const ModulesRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {
    path: 'vaccination',
    children: [{
      path: '',
      loadChildren: () => import('./vaccination/vaccination.module').then(m => m.VaccinationModule)
    }]
  }
];
