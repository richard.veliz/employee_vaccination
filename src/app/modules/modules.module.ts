import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from 'app/shared/components/dashboard/dashboard.component';
import {ModulesRoutes} from "./modules.routing";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ModulesRoutes),
    SharedModule
  ],
  declarations: [
    DashboardComponent,
  ],
  exports: [SharedModule]
})

export class ModulesModule {
}
