import {Constants} from '../constants';
import {AbstractControl, ValidatorFn} from '@angular/forms';
import {validateIdentityCard} from './validateIdentityCard';

export const ValidateIdentityCard = (control: AbstractControl): { [key: string]: boolean } => {
  let response = null;
  if (control.value > 0 && !isNaN(+control.value)) {
    response = (validateIdentityCard(control.value)) ? null : {validateidentitycard: true};
  }

  return response;
};

export const alphaNumeric = (allowedPhrase: string): ValidatorFn => {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value) {
      allowedPhrase = allowedPhrase ? allowedPhrase : '';
      const regEx = new RegExp(Constants.ALPHANUMERIC_PATTERN);
      if (!regEx.test(c.value.replace(allowedPhrase.toUpperCase(), '').replace(allowedPhrase.toLowerCase(), ''))) {
        return {'alphanumeric': true};
      }
    }
    return null;
  };
};

export const numeric = (allowedNumber: string): ValidatorFn => {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value) {
      allowedNumber = allowedNumber ? allowedNumber : '';
      const regEx = new RegExp(Constants.NUMERIC_PATTERN);
      if (!regEx.test(c.value.replace(allowedNumber, ''))) {
        return {'numeric': true};
      }
    }
    return null;
  };
};

export const alphaSpace = (allowedPhrase: string): ValidatorFn => {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value) {
      allowedPhrase = allowedPhrase ? allowedPhrase : '';
      const regEx = new RegExp(Constants.ALPHASPACE_PATTERN);
      if (!regEx.test(c.value.replace(allowedPhrase, ''))) {
        return {'alphaspace': true};
      }
    }
    return null;
  };
};

export const phone = (allowedNumber: string): ValidatorFn => {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value) {
      allowedNumber = allowedNumber ? allowedNumber : '';
      const regEx = new RegExp(Constants.PHONE_PATTERN);
      if (!regEx.test(c.value.replace(allowedNumber, ''))) {
        return {'phone': true};
      }
    }
    return null;
  };
};

export const email = (allowedEmail: string): ValidatorFn => {
  return (c: AbstractControl): { [key: string]: boolean } | null => {
    if (c.value) {
      allowedEmail = allowedEmail ? allowedEmail : '';
      const regEx = new RegExp(Constants.EMAIL_PATTERN);
      if (!regEx.test(c.value.replace(allowedEmail, ''))) {
        return {'email': true};
      }
    }
    return null;
  };
};
