import {Injectable} from '@angular/core';
import {ManagerService} from 'app/shared/services/manager.service';
import {AppConfig} from 'app/app.config';
import {User} from 'app/shared/models/user';

@Injectable()
export class UserService {

  private url = AppConfig.URL_API_DOMAIN + '/users';

  constructor(private managerService: ManagerService) {
  }

  find = (id: number): Promise<any> => this.managerService.get(`${this.url}/${id}`);

  create = (user: User): Promise<any> => this.managerService.post(this.url, user);
}
