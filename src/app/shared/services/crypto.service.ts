import {Injectable} from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({providedIn: 'root'})
export class CryptoService {

  SHA256 = CryptoJS.SHA256;
  AES = CryptoJS.AES;

  key = "clave_secreta";

  encrypt = (value) => this.AES.encrypt(value, this.key).toString();

  decrypt = (value) => this.AES.decrypt(value, this.key).toString(CryptoJS.enc.Utf8);
}
