import {Injectable} from '@angular/core';
import {FormControl, FormGroup, FormArray, ValidatorFn, Validators, ValidationErrors} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  step = 0;

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  getFormValidationErrors(form: FormGroup) {
    const result = [];
    Object.keys(form.controls).forEach(key => {

      const controlErrors: ValidationErrors = form.get(key).errors;
      if (controlErrors) {
        Object.keys(controlErrors).forEach(keyError => {
          result.push({
            'control': key,
            'error': keyError,
            'value': controlErrors[keyError]
          });
        });
      }
    });

    return result;
  }

  setValuesFormGroup(form: FormGroup, data: any) {
    for (const key in form.controls) {
      if (form.controls[key] instanceof FormControl) {
        if (data[key]) {
          const control = <FormControl>form.controls[key];
          this.setValueFormControl(control, data[key]);
        }
      } else if (form.controls[key] instanceof FormGroup) {
        if (data[key]) {
          this.setValuesFormGroup(<FormGroup>form.controls[key], data[key]);
        }
      } else if (form.controls[key] instanceof FormArray) {
        const control = <FormArray>form.controls[key];
        if (data[key]) {
          this.setValuesFormArray(control, data[key]);
        }
      }
    }
  }

  private setValuesFormArray(array: FormArray, data: Array<any>) {
    if (data.length > 0) {
      const clone = array.controls[0];
      array.removeAt(0);
      for (const idx in data) {
        array.push(clone);
        if (clone instanceof FormGroup) {
          this.setValuesFormGroup(<FormGroup>array.controls[idx], data[idx]);
        } else if (clone instanceof FormControl) {
          this.setValueFormControl(<FormControl>array.controls[idx], data[idx]);
        } else if (clone instanceof FormArray) {
          this.setValuesFormArray(<FormArray>array.controls[idx], data[idx]);
        }
      }
    }
  }

  private setValueFormControl(control: FormControl, value: any) {
    control.setValue(value);
  }

  public addRequired(control: FormControl) {
    control.setValidators([Validators.required]);
    control.updateValueAndValidity();
  }

  public removeValidator(control: FormControl) {
    control.setValidators([]);
    control.updateValueAndValidity();
  }

  public addValidator(control: FormControl, validator: ValidatorFn[] | ValidatorFn) {
    control.setValidators(validator);
    control.updateValueAndValidity();
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: FormArray) => {
      const totalSelected = formArray.controls
        .map(control => control.value)
        .reduce((prev, next) => next ? prev + next : prev, 0);

      return totalSelected >= min ? null : {required: true};
    };

    return validator;
  }

  displayFn = (obj?): string | undefined => obj ? obj.name : undefined;

  public compareWith = (option, value): boolean => (value && option) ? option.id === value.id : false;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }
}
