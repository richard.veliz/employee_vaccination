import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {TokenInterface} from 'app/shared/interfaces/token.interface';
import {ManagerService} from "./manager.service";
import {AppConfig} from 'app/app.config';
import {StorageService} from "./storage.service";

@Injectable({providedIn: 'root'})
export class SessionService {

  constructor(private router: Router,
              private storageService: StorageService,
              private managerService: ManagerService) {
  }

  public signin(email: string, password: string) {
    return new Promise((resolve, reject) => {
      return this.managerService.post(AppConfig.URL_TOKEN, {email, password}).then((token: TokenInterface) => {
        this.storeTokenAndUser(token);
        resolve(token);
      }).catch(error => {
        reject(error);
      });
    });
  }


  public logout() {
    this.storageService.clearAll();
    this.router.navigate(['signin']);
  }

  public getUser = () => this.storageService.getUser();

  public getAccessToken = () => this.storageService.getToken();

  public hasValidAccessToken = (): boolean => !!this.getAccessToken();

  private storeTokenAndUser(token: TokenInterface) {
    this.storageService.setToken(token.accessToken);
    if (token.user) {
      this.storageService.setUser(token.user);
    }
  }
}
