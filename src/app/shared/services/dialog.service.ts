import {Injectable} from '@angular/core';
import {MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {ComponentType} from '@angular/cdk/portal';
import {MessageService} from './message.service';
import {SpinnerViewService} from './spinnerview.service';
import {DeleteComponent} from '../components/gui/dialog/delete/delete.component';

@Injectable()
export class DialogService {

  constructor(public dialogRef: MatDialogRef<any>,
              public dialogDelete: MatDialogRef<DeleteComponent>,
              private dialog: MatDialog,
              private dialogConfig: MatDialogConfig,
              private spinnerViewService: SpinnerViewService,
              private messageService: MessageService,
  ) {
    this.dialogConfig.autoFocus = true;
  }

  /**
   * Abre un modal nuevo
   * @param component
   * @param width
   * @param id
   * @param view
   */
  open(component: ComponentType<any>, width: number, data?: any): void {
    this.dialogConfig.width = width + '%';
    this.dialogConfig.data = data;
    this.dialogConfig.maxHeight = '90vh';
    const view = (data) ? data.view : null;
    this.dialogConfig.disableClose = (!view) ? true : (view !== 'show');
    this.dialogRef = this.dialog.open(component, this.dialogConfig);
  }

  /**
   * Cierra el modal nuevo
   * @param result
   */
  close = (result?: any): void => this.dialogRef.close(result);

  /**
   * Abre un modal para eliminar registros
   * @param service
   * @param id
   * @param width
   * @param question
   * @param meserror
   * @param messucces
   */
  delete(service, id: number, width?: number, question?: string, meserror?: string, messucces?: string): void {
    if (!width) {
      width = 35;
    }
    if (!question) {
      question = '¿Esta seguro que desea eliminar el registro?';
    }
    if (!meserror) {
      meserror = 'Hubo un inconveniente al eliminar el registro, por favor comuniquese con el administrador.';
    }
    if (!messucces) {
      messucces = 'Registro eliminado correctamente.';
    }
    this.dialogConfig.disableClose = true;
    this.dialogConfig.width = width + '%';
    this.dialogDelete = this.dialog.open(DeleteComponent, this.dialogConfig);

    const dialogDeleteInstance = this.dialogDelete.componentInstance;
    dialogDeleteInstance.message = question;
    dialogDeleteInstance.onDelete.subscribe(() => {
      this.spinnerViewService.start();
      service.delete(id).then(result => {
          this.messageService.openSnackBar(messucces, null);
          this.dialogDelete.close(1);
          this.spinnerViewService.stop()
        }).catch(error => {
          this.messageService.openSnackBar(meserror, null);
          this.dialogDelete.close();
          this.spinnerViewService.stop()
          console.log(error);
        });
    });
  }
}
