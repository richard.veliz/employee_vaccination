import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {Observable, BehaviorSubject, Subject} from 'rxjs';
import {PaginationInterface} from '../interfaces/pagination.interface';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {WhereInterface} from '../interfaces/where.interface';

export class DatasourceService implements DataSource<any> {

  public subject: BehaviorSubject<any[]>;
  public pageSubject: BehaviorSubject<any>;
  public loadingSubject: BehaviorSubject<boolean>;
  public loading$;
  public totalPage;
  public pageEvent: PageEvent;
  public pageSize = 5;
  public pageSizeOptions = [this.pageSize, 10, 25, 50];
  public refreshPageSubject: Subject<void> = new Subject();
  public changePage: Subject<void> = new Subject();
  public data = [];

  constructor(private service?, private initData?: boolean) {
    this.subject = new BehaviorSubject<any[]>([]);
    this.pageSubject = new BehaviorSubject<any>([]);
    this.loadingSubject = new BehaviorSubject<boolean>(false);
    this.loading$ = this.loadingSubject.asObservable();
    this.totalPage = this.pageSubject;
  }

  connect(collectionViewer: CollectionViewer): Observable<any[]> {
    return this.subject.asObservable();
  }

  disconnect(): void {
    this.subject.unsubscribe();
    this.loadingSubject.unsubscribe();
    this.pageSubject.unsubscribe();
    this.refreshPageSubject.unsubscribe();
    this.changePage.unsubscribe();
  }

  loadCollection(pagination: PaginationInterface, useCache = false) {
    this.loadingSubject.next(true);
    this.service.collection(pagination, useCache)
      .then((result: any) => {
        this.loadingSubject.next(true);
        const data = result.body;
        const totalCount = result.headers.get('X-Total-Count');
        if (data) {
          this.data = data;
          this.subject.next(data);
          this.pageSubject.next([totalCount]);
        }
      }).catch(error => console.log(error))
      .finally(() => this.loadingSubject.next(false));
  }

  loadPage(page: number = 1, limit: number = this.pageSize, where: WhereInterface[] = []) {
    const pagination: PaginationInterface = {page, limit, where};
    (!this.initData) ? this.loadCollection(pagination) : this.subject.next(this.data);
  }

  onPaginateChange = () => this.changePage.next();

  refreshPage(paginator: MatPaginator, pageIndex?: number, where: WhereInterface[] = []) {
    if (!pageIndex) {
      pageIndex = paginator.pageIndex + 1;
    }
    this.loadPage(pageIndex, paginator.pageSize, where);
  }
}
