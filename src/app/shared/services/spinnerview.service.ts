import {Injectable} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {SpinnerViewComponent} from "../components/gui/spinner/spinnerview.component";


@Injectable({providedIn: 'root'})
export class SpinnerViewService {

  private dialogRef: MatDialogRef<SpinnerViewComponent>;
  public isStart = false;
  private openDialogs: MatDialogRef<any>[];

  constructor(private _dialog: MatDialog) {
    this.openDialogs = _dialog.openDialogs;
  }

  start(message?: string, hideDialog?: boolean): MatDialogRef<SpinnerViewComponent> {
    if (!this.isStart) {

      if (hideDialog === undefined || hideDialog) {
        this.openDialogs.forEach(dialog => {
          dialog.addPanelClass('hidden-dialog-container');
        });
      }

      const dialogRef = this._dialog.open(SpinnerViewComponent, {
        disableClose: true,
        data: message === null ||message === '' || message === undefined ? 'Procesando...' : message,
        panelClass: 'spinner-dialog-container'
      });
      this.dialogRef = dialogRef;
      this.isStart = true;
      return dialogRef;
    }
  };

  stop(ref?: MatDialogRef<SpinnerViewComponent>) {
    (ref !== undefined) ? ref.close() : (this.dialogRef !== undefined ? this.dialogRef.close() : null);
    this.isStart = false;
    this.openDialogs.forEach(dialog => {
      dialog.removePanelClass('hidden-dialog-container');
    });
  }
}
