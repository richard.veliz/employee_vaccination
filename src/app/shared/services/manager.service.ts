import {Injectable} from '@angular/core';
import {PaginationInterface} from '../interfaces/pagination.interface';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({providedIn: 'root'})

export class ManagerService {

  constructor(private http: HttpClient) {
  }

  post = (url: string, obj: object = null): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.http.post(url, obj).pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };

  put = (url: string, obj: object = null): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.http.put(url, obj).pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };

  patch = (url: string, body: object = null): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.http.patch(url, body).pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };

  delete = (url: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.http.delete(url).pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };

  get = (url: string, responseType: string = 'json'): Promise<any> => {
    const requestOptions: object = {
      responseType
    };
    return new Promise((resolve, reject) => {
      this.http.get(url, requestOptions).pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };

  /**
   * Obtiene los datos paginados del api
   * @param url
   * @param pagination
   */
  collection = (url: string, pagination: PaginationInterface): Promise<any> => {
    let params = new HttpParams()
      .set('_page', pagination.page.toString())
      .set('_limit', pagination.limit.toString());

    if (pagination.where.length > 0) {
      params = new HttpParams();
      pagination.where.map(row => {
        params = params.append(row.key, row.value)
      });
      params = params.append('_limit', pagination.limit.toString());
    }

    return new Promise((resolve, reject) => {
      this.http.get(url, {params, observe: 'response'})
        .pipe().toPromise()
        .then((response) => {
          resolve(response);
        }).catch((error) => {
        console.log(error);
        reject(error);
      });
    });
  };
}
