import {throwError as observableThrowError, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable, Injector} from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpSentEvent,
  HttpHeaderResponse,
  HttpProgressEvent,
  HttpResponse,
  HttpUserEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import {Router} from '@angular/router';
import {SessionService} from "./session.service";
import {AppConfig} from "../../app.config";

@Injectable({providedIn: 'root'})
export class InterceptorService implements HttpInterceptor {
  sesionService: SessionService;

  constructor(private router: Router, private inj: Injector) {
    this.sesionService = this.inj.get(SessionService);
  }

  addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + token
      }
    });
  }

// El RequestInterceptorService implementará HttpInterceptor que tiene solo un método: interceptar.
// Agregará un token al encabezado en cada llamada y detectará cualquier error que pueda ocurrir.
  intercept(request: HttpRequest<any>, next: HttpHandler):
    Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
    if (this.isValidRequestForInterceptor(request.url)) {
      return next.handle(this.addToken(request.clone(), this.sesionService.getAccessToken())).pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            switch ((<HttpErrorResponse>error).status) {
              case 401:
                return this.handleCodeError401(request.clone(), next, error.error);
              default:
                return observableThrowError(error);
            }
          } else {
            return observableThrowError(error);
          }
        }));
    }

    return next.handle(request);
  }

  private isValidRequestForInterceptor(requestUrl: string): boolean {
    return !!(requestUrl.indexOf(AppConfig.URL_API_DOMAIN) === -1 || requestUrl !== AppConfig.URL_TOKEN);
  }

  handleCodeError401(request: HttpRequest<any>, next: HttpHandler, error) {
    this.sesionService.logout();
    return observableThrowError('Ha sido cerrada su inicio de sesión');
  }
}
