import {Injectable} from '@angular/core';
import {CryptoService} from './crypto.service';
import {isJsonString} from 'app/shared/helpers/string.helper';
import {User} from "../models/user";
import {AppConfig} from 'app/app.config';

@Injectable({providedIn: 'root'})
export class StorageService {
  TOKEN = 'access_token';
  USER = 'user';

  constructor(private cryptoService: CryptoService) {
  }

  public setStorage(key, value, inSessionStorage = true, encrypt = true) {
    if (value) {
      value = (AppConfig.ENCRYPT_STORAGE && encrypt) ? this.encrypt(value) : value;
      if (typeof value === 'object') {
        value = JSON.stringify(value);
      }
      (inSessionStorage) ? sessionStorage.setItem(key, value) : localStorage.setItem(key, value);
    }
  }

  public getStorage(key, inSessionStorage = true, decrypt = true): any {
    let value = (inSessionStorage) ? sessionStorage.getItem(key) : localStorage.getItem(key);
    if (value) {

      if (isJsonString(value)) {
        value = JSON.parse(value);
      }
      if (AppConfig.ENCRYPT_STORAGE && decrypt) {
        value = this.decrypt(value);
      }
      return value;
    }
    return null;
  }

  private encrypt = (value) => {
    if (typeof value == 'object') {
      for (let key in value) {
        value[key] = this.encrypt(value[key]);
      }
    } else {
      value = this.cryptoService.encrypt(value);
    }
    return value;
  };

  private decrypt = (value) => {
    if (typeof value == 'object') {
      for (let key in value) {
        value[key] = this.decrypt(value[key]);
      }
    } else {
      this.cryptoService.decrypt(value);
    }
    return value;
  };

  public setToken = (token: string) => this.setStorage(this.TOKEN, token);
  public getToken = (): string => this.getStorage(this.TOKEN);

  public removeItem = (key, inSessionStorage = false) => (inSessionStorage) ? sessionStorage.removeItem(key) : localStorage.removeItem(key);

  public setUser = (user: User) => this.setStorage(this.USER, user);
  public getUser = () => this.getStorage(this.USER);

  public clearAll() {
    sessionStorage.clear();
    localStorage.clear();
  }
}
