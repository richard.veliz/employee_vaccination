import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NumberDirective } from './number.directive';
import { NumberandletterDirective } from './numberandletter.directive';
import { LetterDirective } from './letter.directive';


const directives = [
  NumberDirective,
  NumberandletterDirective,
  LetterDirective,
]

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: directives,
  exports: directives
})
export class SharedDirectivesModule {}
