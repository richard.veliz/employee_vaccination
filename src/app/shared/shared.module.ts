import {CUSTOM_ELEMENTS_SCHEMA, ErrorHandler, LOCALE_ID, NgModule} from '@angular/core';

// MODULES
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {CommonModule, DatePipe, registerLocaleData} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MatPaginatorIntl} from '@angular/material/paginator';
// import * as echarts from 'echarts';

// SERVICES

// COMPONENTS


// DIRECTIVES

// PROVIDER
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
// import {adapterFactory} from 'angular-calendar/date-adapters/moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import {MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import localeEsEC from '@angular/common/locales/es-EC';
import {ComponentsModule} from './components/components.module';
import {AuthGuard} from './guards/auth.guard';
import {InterceptorService} from './services/interceptor.service';
import {getSpanishPaginatorIntl} from './helpers/spanish-paginator-intl';
import {MOMENT_DATE_FORMATS} from './constants';
import {MomentDateAdapter} from './helpers/moment-date-adapter';
import {DialogService} from './services/dialog.service';

registerLocaleData(localeEsEC, 'es-EC');


@NgModule({
  declarations: [],
  exports: [
    RouterModule,
    HttpClientModule,
    ComponentsModule,
  ],
  imports: [
    HttpClientModule,
    ComponentsModule,
  ],
  providers: [
    DatePipe,
    MatDialogConfig,
    AuthGuard,
    {provide: MatDialogRef, useValue: {}},
    {provide: MAT_DIALOG_DATA, useValue: []},
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true},
    {provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}},
    {provide: MatPaginatorIntl, useValue: getSpanishPaginatorIntl()},
    {provide: MAT_DATE_LOCALE, useValue: 'es_EC'},
    {provide: MAT_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS},
    {provide: DateAdapter, useClass: MomentDateAdapter},
    {provide: LOCALE_ID, useValue: 'es-EC'},
    DialogService
  ]
})

export class SharedModule {
}
