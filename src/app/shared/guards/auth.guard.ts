import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {SessionService} from '../services/session.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,
                private sessionService: SessionService) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.sessionService.hasValidAccessToken()) {
            return true;
        } else {
            this.router.navigate(['/signin']);
            return false;
        }
    }
}
