import {MatDateFormats} from '@angular/material/core';

export class Constants {
  public static EMAIL_PATTERN = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  public static ALPHANUMERIC_PATTERN = /^[a-zA-Z0-9]*$/;
  public static NUMERIC_PATTERN = /^-?[\d.]+(?:e-?\d+)?$/;
  public static ALPHASPACE_PATTERN = /^[a-zA-Z áéíóúñü]+$/;
  public static PHONE_PATTERN = /[0-9\+\-\ ]/;
  public static ROL_ADMIN = 1;
  public static ROL_EMPLOYEE = 2;
}

export const MOMENT_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: 'DD-MM-YYYY'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  }
};
