import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Validators, FormGroup, FormControl} from '@angular/forms';
import {SessionService} from 'app/shared/services/session.service';
import {FormService} from 'app/shared/services/form.service';
import {SpinnerViewService} from "app/shared/services/spinnerview.service";
import {MessageService} from "app/shared/services/message.service";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private sessionService: SessionService,
              private formService: FormService,
              private messageService: MessageService,
              private spinnerViewService: SpinnerViewService,
              private router: Router) {
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });

  }

  async signin() {
    this.formService.validateAllFormFields(this.loginForm);
    if (this.loginForm.valid) {
      this.spinnerViewService.start();
      const signinData = this.loginForm.value;
      this.sessionService.signin(signinData.username, signinData.password).then(() => {
        this.spinnerViewService.stop();
        this.router.navigate(['/dashboard']);
      }).catch(error => {
        this.messageService.showError("Usuario o contraseña no valida");
        this.spinnerViewService.stop();
      });
    }
  }
}
