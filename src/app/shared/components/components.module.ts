import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {FooterComponent} from './layouts/footer/footer.component';
import {NavbarComponent} from './layouts/navbar/navbar.component';
import {SidebarComponent} from './layouts/sidebar/sidebar.component';
import {SigninComponent} from './sessions/signin/signin.component';
import {MaterialModule} from '../material.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NotFoundComponent} from './sessions/notfound/notfound.component';
import {AdminLayoutComponent} from './layouts/adminlayout/adminlayout.component';
import {SpinnerViewComponent} from './gui/spinner/spinnerview.component';
import {IconComponent} from './gui/datatable/icon/icon.component';
import {PaginatorComponent} from './gui/datatable/paginator/paginator.component';
import {HeadComponent} from './gui/dialog/head/head.component';
import {DeleteComponent} from './gui/dialog/delete/delete.component';
import {SharedDirectivesModule} from '../directives/shared-directives.module';
import {NgxMaskModule} from "ngx-mask";


const components = [
  FooterComponent,
  NavbarComponent,
  SidebarComponent,
  AdminLayoutComponent,
  SigninComponent,
  NotFoundComponent,
  SpinnerViewComponent,
  IconComponent,
  PaginatorComponent,
  HeadComponent,
  DeleteComponent
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    SharedDirectivesModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [components],
  exports: [
    components,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    SharedDirectivesModule,
    NgxMaskModule
  ]
})
export class ComponentsModule {
}
