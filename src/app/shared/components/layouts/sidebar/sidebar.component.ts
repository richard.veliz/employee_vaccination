import {Component, OnInit} from '@angular/core';
import {SessionService} from "app/shared/services/session.service";
import {User} from "app/shared/models/user";
import {MenuInterface} from "app/shared/interfaces/menu.interface";
import {Constants} from "app/shared/constants";

export const ListMenu: MenuInterface[] = [
  {path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '', rolId: null},
  {path: '/vaccination/tableemployee', title: 'Empleado', icon: 'content_paste', class: '', rolId: Constants.ROL_ADMIN},
  {path: '/vaccination/updateemployee', title: 'Actualizar datos', icon: 'person', class: '', rolId: Constants.ROL_EMPLOYEE},
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public menuItems: any[];
  public user: User;
  public constants = Constants;

  constructor(public sessionService: SessionService) {
  }

  ngOnInit() {
    this.menuItems = ListMenu.filter(menuItem => menuItem);
    this.user = this.sessionService.getUser();
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
