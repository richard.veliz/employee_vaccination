import {Component, Input, OnInit} from '@angular/core';
import {toBoolean} from 'app/shared/helpers/string.helper';


@Component({
  selector: 'app-dialog-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css'],
})
export class HeadComponent implements OnInit {

  @Input() title: string;
  @Input() subtitle: string;
  @Input() color: string;
  @Input() close: boolean;


  ngOnInit() {
    this.color = (this.color) ? this.color : 'primary';
    this.title = (this.title) ? this.title : '';
    this.subtitle = (this.subtitle) ? this.subtitle : '';
    this.close = (this.close) ? toBoolean(this.close) : true;
  }
}
