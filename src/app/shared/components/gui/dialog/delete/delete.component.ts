import {Component, Output, EventEmitter, Input, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-dialog-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css'],
})
export class DeleteComponent implements OnDestroy {

  @Output() onDelete: EventEmitter<void> = new EventEmitter<void>();
  @Input() message: string;

  delete() {
    this.onDelete.emit();
  }

  ngOnDestroy(): void {
    this.onDelete.unsubscribe();
  }
}
