import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-datatable-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css']
})
export class IconComponent implements OnInit {

  @Input() title = '';
  @Input() icon = 'visibility';
  @Input() color = '';
  @Input() class = '';

  ngOnInit() {
    const classOrigin = this.class;
    if (classOrigin === 'show') {
      this.title = 'Consultar';
      this.color = 'primary';
      this.icon = 'visibility';
    } else if (classOrigin === 'edit') {
      this.title = 'Editar';
      this.icon = 'edit';
    } else if (classOrigin === 'delete') {
      this.title = 'Eliminar';
      this.color = 'warn';
      this.icon = 'delete';
    } else if (classOrigin === 'grant') {
      if (!this.title) {
        this.title = 'Permisos';
      }
      this.color = 'accent';
      this.icon = 'vpn_key';
    } else if (classOrigin === 'export') {
      if (!this.title) {
        this.title = 'Exportar';
      }
      this.color = 'accent';
      this.icon = 'picture_as_pdf';
    } else if (classOrigin === 'clone') {
      if (!this.title) {
        this.title = 'Clonar';
      }
      this.color = 'accent';
      this.icon = 'content_copy';
    }
  }
}
