import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {DatasourceService} from "app/shared/services/datasource.service";

@Component({
  selector: 'app-datatable-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Output() @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @Input() dataSource: DatasourceService;
  @Input() pageSize: number;

  constructor() {
  }

  ngOnInit() {
    if (!this.pageSize) {
      this.pageSize = this.dataSource.pageSize;
    }
  }
}
