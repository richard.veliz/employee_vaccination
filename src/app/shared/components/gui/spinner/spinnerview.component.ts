import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Router} from '@angular/router';

@Component({
  selector: 'app-spinner-view',
  templateUrl: './spinnerview.component.html',
  styleUrls: ['./spinnerview.component.css']
})
export class SpinnerViewComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SpinnerViewComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private router: Router) {
  }

  ngOnInit() {

  }

}
