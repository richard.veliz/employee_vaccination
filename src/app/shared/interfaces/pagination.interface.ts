import {WhereInterface} from "./where.interface";

export interface PaginationInterface {
  page: number;
  limit: number;
  where: WhereInterface[];
}
